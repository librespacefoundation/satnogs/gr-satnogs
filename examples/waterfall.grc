options:
  parameters:
    author: surligas
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: waterfall_sink_example
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: Waterfall Sink Example
    window_size: (1000,1000)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [248, 16.0]
    rotation: 0
    state: enabled
- name: analog_noise_source_x_0
  id: analog_noise_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '0.01'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    noise_type: analog.GR_GAUSSIAN
    seed: '0'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [584, 404.0]
    rotation: 0
    state: true
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: samp_rate/20
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    showports: 'False'
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [592, 260.0]
    rotation: 0
    state: true
- name: blocks_add_xx_0
  id: blocks_add_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [896, 312.0]
    rotation: 0
    state: true
- name: blocks_head_0
  id: blocks_head
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_items: 1024*2000
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1200, 296.0]
    rotation: 0
    state: enabled
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    limit: auto
    maximum: '0.1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1072, 432.0]
    rotation: 0
    state: enabled
- name: output_filename
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/waterfall.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 196.0]
    rotation: 0
    state: true
- name: satnogs_waterfall_sink_0
  id: satnogs_waterfall_sink
  parameters:
    affinity: ''
    alias: ''
    center_freq: '0.0'
    comment: ''
    fft_size: '1024'
    filename: output_filename
    mode: '1'
    rps: '10'
    samp_rate: samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1464, 316.0]
    rotation: 0
    state: true

connections:
- [analog_noise_source_x_0, '0', blocks_add_xx_0, '1']
- [analog_sig_source_x_0, '0', blocks_add_xx_0, '0']
- [blocks_add_xx_0, '0', blocks_throttle_0, '0']
- [blocks_head_0, '0', satnogs_waterfall_sink_0, '0']
- [blocks_throttle_0, '0', blocks_head_0, '0']

metadata:
  file_format: 1
  grc_version: v3.10.5.1
