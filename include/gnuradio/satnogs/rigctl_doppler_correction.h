/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RIGCTL_DOPPLER_CORRECTION_H
#define RIGCTL_DOPPLER_CORRECTION_H


#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/doppler_correction.h>
#include <hamlib/rigclass.h>

namespace gr {
namespace satnogs {

class SATNOGS_API rigctl_doppler_correction : virtual public doppler_correction
{
public:
    static doppler_correction::sptr
    make(double center_freq, rig_model_t model, vfo_t vfo = RIG_VFO_CURR);
    void start() override;

    void stop() override;

    double offset() override;

    void reset() override;

    void set_vfo(vfo_t vfo);

    void set_param(const std::string& name, const std::string& val);

protected:
    rigctl_doppler_correction(double center_freq,
                              rig_model_t model,
                              vfo_t vfo = RIG_VFO_CURR);

private:
    const double m_freq;
    vfo_t m_vfo;
    Rig m_rig;
};

} // namespace satnogs
} // namespace gr

#endif // RIGCTL_DOPPLER_CORRECTION_H
