/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2021-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDED_SATNOGS_AX100_MODE5_ENCODER_H
#define INCLUDED_SATNOGS_AX100_MODE5_ENCODER_H

#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/crc.h>
#include <gnuradio/satnogs/encoder.h>
#include <gnuradio/satnogs/whitening.h>
#include <vector>

namespace gr {
namespace satnogs {

/*!
 * \brief This encoder implements the AX100 mode 5 framing and coding scheme
 *
 */
class SATNOGS_API ax100_mode5_encoder : public encoder
{
public:
    using sptr = std::shared_ptr<ax100_mode5_encoder>;

    /**
     * @brief Coded Length field length in bytes
     *
     */
    static constexpr size_t length_size = 3;

    static sptr make(const std::vector<uint8_t>& preamble,
                     const std::vector<uint8_t>& sync,
                     crc::type crc = crc::type::CRC32_C,
                     whitening::sptr scrambler = whitening::make_ccsds(),
                     bool enable_rs = true);

    ax100_mode5_encoder(const std::vector<uint8_t>& preamble,
                        const std::vector<uint8_t>& sync,
                        crc::type crc,
                        whitening::sptr scrambler,
                        bool enable_rs);
    ~ax100_mode5_encoder();

    pmt::pmt_t encode(pmt::pmt_t msg);

private:
    const crc::type d_crc;
    const bool d_rs;
    const size_t d_payload_start;
    whitening::sptr d_scrambler;
    uint8_t* d_pdu;
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_AX100_MODE5_ENCODER_H */
