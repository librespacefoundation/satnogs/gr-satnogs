/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/attributes.h>
#include <gnuradio/satnogs/crc.h>
#include <boost/test/unit_test.hpp>
#include <random>

namespace gr::satnogs {

BOOST_AUTO_TEST_CASE(crcpp_compat)
{
    std::vector<uint8_t> data_in = {
        'H', 'E', 'L', 'L', 'O', ' ', 'W', 'O', 'R', 'L', 'D'
    };

    auto crc32_c = CRC::Calculate(data_in.data(), data_in.size(), CRC::CRC_32_C());
    auto crc16_x25 = CRC::Calculate(data_in.data(), data_in.size(), CRC::CRC_16_X25());
    auto crc16_xmodem =
        CRC::Calculate(data_in.data(), data_in.size(), CRC::CRC_16_XMODEM());
    auto crc16_cms = CRC::Calculate(data_in.data(), data_in.size(), CRC::CRC_16_CMS());

    /* This was assumed wrongly as CRC16-AUG. The original does not XOR the output */
    const CRC::Parameters<crcpp_uint16, 16> wrong_aug_ccitt = {
        0x1021, 0x1D0F, 0xFFFF, false, false
    };
    auto crc16_aug_xored =
        CRC::Calculate(data_in.data(), data_in.size(), wrong_aug_ccitt);

    /* Check compatibility with the old gr-satnogs crc functions */
    const uint32_t old_crc32_c = 0xc481333f;
    const uint32_t old_crc16_ax25 = 0x2cbb;
    const uint32_t old_crc16_ccitt = 0x8149;
    const uint32_t old_crc16_ibm = 0x3ecf;
    const uint32_t old_crc16_aug = 0x56ba;

    BOOST_REQUIRE_EQUAL(crc32_c, old_crc32_c);
    BOOST_REQUIRE_EQUAL(crc16_x25, old_crc16_ax25);
    BOOST_REQUIRE_EQUAL(crc16_xmodem, old_crc16_ccitt);
    BOOST_REQUIRE_EQUAL(crc16_cms, old_crc16_ibm);
    BOOST_REQUIRE_EQUAL(crc16_aug_xored, old_crc16_aug);
}

BOOST_AUTO_TEST_CASE(crc_size)
{
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC16_AUG_CCITT), 2);
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC16_AUG_CCITT_XOR), 2);
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC16_KERMIT), 2);
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC16_HDLC), 2);
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC16_CMS), 2);
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC16_XMODEM), 2);
    BOOST_REQUIRE_EQUAL(crc::size(crc::type::CRC32_C), 4);

    /* Test also with the custom CRC interface */
    auto crc8 = CRC::CRC_8_WCDMA();
    BOOST_REQUIRE_EQUAL(crc::size(crc8), 1);
    auto crc16 = CRC::CRC_16_ARC();
    BOOST_REQUIRE_EQUAL(crc::size(crc16), 2);
    auto crc32 = CRC::CRC_32_BZIP2();
    BOOST_REQUIRE_EQUAL(crc::size(crc32), 4);
}

BOOST_AUTO_TEST_CASE(crc_check_simple)
{
    const size_t frame_len = 1024;
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    /* Allocate enough space for the overhead */
    std::vector<uint8_t> data_in(frame_len + crc::size(crc::type::CRC16_AUG_CCITT), 0);
    std::generate_n(data_in.begin(), frame_len, gen);

    crc::append(crc::type::CRC16_AUG_CCITT,
                data_in.data() + frame_len,
                data_in.data(),
                frame_len);
    BOOST_REQUIRE_EQUAL(crc::check(crc::type::CRC16_AUG_CCITT,
                                   data_in.data(),
                                   frame_len + crc::size(crc::type::CRC16_AUG_CCITT)),
                        true);
}

BOOST_AUTO_TEST_CASE(custom_crc_check_simple)
{
    const size_t frame_len = 1024;
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    /* Allocate enough space for the overhead */
    std::vector<uint8_t> data_in(frame_len + crc::size(CRC::CRC_16_XMODEM()), 0);
    std::generate_n(data_in.begin(), frame_len, gen);

    crc::append(
        CRC::CRC_16_XMODEM(), data_in.data() + frame_len, data_in.data(), frame_len);
    BOOST_REQUIRE_EQUAL(crc::check(crc::type::CRC16_XMODEM,
                                   data_in.data(),
                                   frame_len + crc::size(crc::type::CRC16_XMODEM)),
                        true);
}

BOOST_AUTO_TEST_CASE(crc_check_full)
{
    const size_t frame_len = 1024;
    size_t overhead = 0;
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    /* Allocate enough space for the overhead */
    std::vector<uint8_t> data_in(frame_len + 512, 0);
    std::generate_n(data_in.begin(), frame_len, gen);

    overhead += crc::append(crc::type::CRC16_AUG_CCITT,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(crc::type::CRC16_AUG_CCITT_XOR,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(crc::type::CRC16_CMS,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(crc::type::CRC16_HDLC,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(crc::type::CRC16_KERMIT,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(crc::type::CRC16_XMODEM,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(crc::type::CRC32_C,
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);

    /* Test also custom CRC types */
    overhead += crc::append(CRC::CRC_8_WCDMA(),
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(CRC::CRC_16_ARC(),
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);
    overhead += crc::append(CRC::CRC_32_BZIP2(),
                            data_in.data() + frame_len + overhead,
                            data_in.data(),
                            frame_len + overhead);


    BOOST_REQUIRE_EQUAL(
        crc::check(CRC::CRC_32_BZIP2(), data_in.data(), frame_len + overhead), true);
    overhead -= crc::size(CRC::CRC_32_BZIP2());
}

} // namespace gr::satnogs