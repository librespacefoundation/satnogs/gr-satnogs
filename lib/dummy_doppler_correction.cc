/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/satnogs/dummy_doppler_correction.h>

namespace gr {
namespace satnogs {

doppler_correction::sptr dummy_doppler_correction::make(double offset)
{
    return doppler_correction::sptr(new dummy_doppler_correction(offset));
}

void dummy_doppler_correction::start() {}

void dummy_doppler_correction::stop() {}

double dummy_doppler_correction::offset() { return m_offset; }

void dummy_doppler_correction::reset() {}

dummy_doppler_correction::dummy_doppler_correction(double offset)
    : doppler_correction("dummy"), m_offset(offset)
{
}


} // namespace satnogs
} // namespace gr