/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "crc_async_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace satnogs {


crc_async::sptr crc_async::make(crc::type crc, bool check, bool nbo)
{
    return gnuradio::make_block_sptr<crc_async_impl>(crc, check, nbo);
}

template <typename CRCType, crcpp_uint16 CRCWidth>
crc_async::sptr
crc_async::make(const CRC::Parameters<CRCType, CRCWidth>& crc, bool check, bool nbo)
{
    return gnuradio::make_block_sptr<crc_async_impl>(crc, check, nbo);
}

/* Specialization is needed for the pybind11 and should be done for every type of CRC  */
template <>
crc_async::sptr
crc_async::make(const CRC::Parameters<crcpp_uint8, 8>& crc, bool check, bool nbo)
{
    return gnuradio::make_block_sptr<crc_async_impl>(crc, check, nbo);
}


template <>
crc_async::sptr
crc_async::make(const CRC::Parameters<crcpp_uint16, 16>& crc, bool check, bool nbo)
{
    return gnuradio::make_block_sptr<crc_async_impl>(crc, check, nbo);
}

template <>
crc_async::sptr
crc_async::make(const CRC::Parameters<crcpp_uint32, 32>& crc, bool check, bool nbo)
{
    return gnuradio::make_block_sptr<crc_async_impl>(crc, check, nbo);
}

/*
 * The private constructor
 */
template <typename CRCType, crcpp_uint16 CRCWidth>
crc_async_impl::crc_async_impl(const CRC::Parameters<CRCType, CRCWidth>& crc,
                               bool check,
                               bool nbo)
    : gr::block(
          "crc_async", gr::io_signature::make(0, 0, 0), gr::io_signature::make(0, 0, 0)),
      m_nbo(nbo)
{
    this->message_port_register_in(pmt::mp("in"));
    this->message_port_register_out(pmt::mp("out"));

    if (check) {
        this->set_msg_handler(pmt::mp("in"),
                              [this, crc](pmt::pmt_t msg) { this->check(crc, msg); });
    } else {
        this->set_msg_handler(pmt::mp("in"),
                              [this, crc](pmt::pmt_t msg) { this->append(crc, msg); });
    }
}

/*
 * The private constructor
 */
crc_async_impl::crc_async_impl(crc::type crc, bool check, bool nbo)
    : gr::block(
          "crc_async", gr::io_signature::make(0, 0, 0), gr::io_signature::make(0, 0, 0)),
      m_nbo(nbo)
{
    this->message_port_register_in(pmt::mp("in"));
    this->message_port_register_out(pmt::mp("out"));

    if (check) {
        this->set_msg_handler(pmt::mp("in"),
                              [this, crc](pmt::pmt_t msg) { this->check(crc, msg); });
    } else {
        this->set_msg_handler(pmt::mp("in"),
                              [this, crc](pmt::pmt_t msg) { this->append(crc, msg); });
    }
}

/*
 * Our virtual destructor.
 */
crc_async_impl::~crc_async_impl() {}

int crc_async_impl::general_work(int noutput_items,
                                 gr_vector_int& ninput_items,
                                 gr_vector_const_void_star& input_items,
                                 gr_vector_void_star& output_items)
{
    return 0;
}

template <typename CRCType, crcpp_uint16 CRCWidth>
void crc_async_impl::append(const CRC::Parameters<CRCType, CRCWidth>& crc, pmt::pmt_t msg)
{
    // extract input pdu
    pmt::pmt_t meta(pmt::car(msg));
    pmt::pmt_t bytes(pmt::cdr(msg));

    size_t pkt_len(0);
    const uint8_t* bytes_in = pmt::u8vector_elements(bytes, pkt_len);
    std::vector<uint8_t> bytes_out(crc::size(crc) + pkt_len);

    std::copy(bytes_in, bytes_in + pkt_len, bytes_out.begin());
    pkt_len += crc::append(crc, bytes_out.data() + pkt_len, bytes_in, pkt_len, m_nbo);

    pmt::pmt_t output = pmt::init_u8vector(
        pkt_len,
        bytes_out.data()); // this copies the values from bytes_out into the u8vector
    pmt::pmt_t msg_pair = pmt::cons(meta, output);

    this->message_port_pub(pmt::mp("out"), msg_pair);
}

template <typename CRCType, crcpp_uint16 CRCWidth>
void crc_async_impl::check(const CRC::Parameters<CRCType, CRCWidth>& crc, pmt::pmt_t msg)
{
    // extract input pdu
    pmt::pmt_t meta(pmt::car(msg));
    pmt::pmt_t bytes(pmt::cdr(msg));

    size_t pkt_len(0);
    const uint8_t* bytes_in = pmt::u8vector_elements(bytes, pkt_len);
    bool ok = crc::check(crc, bytes_in, pkt_len, m_nbo);
    if (!ok) {
        d_logger->debug("Wrong CRC");
        return;
    }

    pmt::pmt_t output = pmt::init_u8vector(pkt_len - crc::size(crc), bytes_in);
    pmt::pmt_t msg_pair = pmt::cons(meta, output);
    this->message_port_pub(pmt::mp("out"), msg_pair);
}

void crc_async_impl::append(crc::type crc, pmt::pmt_t msg)
{
    // extract input pdu
    pmt::pmt_t meta(pmt::car(msg));
    pmt::pmt_t bytes(pmt::cdr(msg));

    size_t pkt_len(0);
    const uint8_t* bytes_in = pmt::u8vector_elements(bytes, pkt_len);
    std::vector<uint8_t> bytes_out(crc::size(crc) + pkt_len);

    std::copy(bytes_in, bytes_in + pkt_len, bytes_out.begin());
    pkt_len += crc::append(crc, bytes_out.data() + pkt_len, bytes_in, pkt_len, m_nbo);

    pmt::pmt_t output = pmt::init_u8vector(
        pkt_len,
        bytes_out.data()); // this copies the values from bytes_out into the u8vector
    pmt::pmt_t msg_pair = pmt::cons(meta, output);

    this->message_port_pub(pmt::mp("out"), msg_pair);
}

void crc_async_impl::check(crc::type crc, pmt::pmt_t msg)
{
    // extract input pdu
    pmt::pmt_t meta(pmt::car(msg));
    pmt::pmt_t bytes(pmt::cdr(msg));

    size_t pkt_len(0);
    const uint8_t* bytes_in = pmt::u8vector_elements(bytes, pkt_len);
    bool ok = crc::check(crc, bytes_in, pkt_len, m_nbo);
    if (!ok) {
        d_logger->debug("Wrong CRC");
        return;
    }

    pmt::pmt_t output = pmt::init_u8vector(pkt_len - crc::size(crc), bytes_in);
    pmt::pmt_t msg_pair = pmt::cons(meta, output);
    this->message_port_pub(pmt::mp("out"), msg_pair);
}


} /* namespace satnogs */
} /* namespace gr */
