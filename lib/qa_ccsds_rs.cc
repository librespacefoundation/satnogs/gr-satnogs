/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/attributes.h>
#include <gnuradio/satnogs/code/ccsds_rs_decoder.h>
#include <gnuradio/satnogs/code/ccsds_rs_encoder.h>
#include <unordered_set>
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <random>

namespace gr {
namespace satnogs {
namespace code {

BOOST_AUTO_TEST_CASE(simple)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(223, 0);
    std::generate_n(data_in.begin(), 223, gen);
    std::vector<uint8_t> data_coded(255, 0);
    std::vector<uint8_t> data_decoded(223, 0);

    auto enc = ccsds_rs_encoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    auto dec = ccsds_rs_decoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    enc->set_frame_size(223);
    dec->set_frame_size(255);
    enc->generic_work(data_in.data(), data_coded.data());
    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

BOOST_AUTO_TEST_CASE(simple_vfill)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(100, 0);
    std::generate_n(data_in.begin(), 100, gen);
    std::vector<uint8_t> data_coded(100 + 32, 0);
    std::vector<uint8_t> data_decoded(100, 0);

    auto enc = ccsds_rs_encoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    auto dec = ccsds_rs_decoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    enc->set_frame_size(100);
    dec->set_frame_size(100 + 32);
    enc->generic_work(data_in.data(), data_coded.data());
    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

BOOST_AUTO_TEST_CASE(max_errors_simple)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(223, 0);
    std::generate_n(data_in.begin(), 223, gen);
    std::vector<uint8_t> data_coded(255, 0);
    std::vector<uint8_t> data_decoded(223, 0);

    auto enc = ccsds_rs_encoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    auto dec = ccsds_rs_decoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    enc->set_frame_size(223);
    dec->set_frame_size(255);
    enc->generic_work(data_in.data(), data_coded.data());

    std::unordered_set<uint8_t> idx;
    while (idx.size() < 16) {
        idx.insert(uni(mt));
    }
    for (auto elem : idx) {
        data_coded[elem] = ~data_coded[elem];
    }

    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

BOOST_AUTO_TEST_CASE(max_errors_vfill)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    std::uniform_int_distribution<uint8_t> uni_idx(0, 100);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(100, 0);
    std::generate_n(data_in.begin(), 100, gen);
    std::vector<uint8_t> data_coded(100 + 32, 0);
    std::vector<uint8_t> data_decoded(100, 0);

    auto enc = ccsds_rs_encoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    auto dec = ccsds_rs_decoder::make(ccsds_rs_encoder::ecc::ecc16,
                                      ccsds_rs_encoder::interleaving_depth::depth1);
    enc->set_frame_size(100);
    dec->set_frame_size(100 + 32);
    enc->generic_work(data_in.data(), data_coded.data());

    std::unordered_set<uint8_t> idx;
    while (idx.size() < 16) {
        idx.insert(uni_idx(mt));
    }
    for (auto elem : idx) {
        data_coded[elem] = ~data_coded[elem];
    }

    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

void test_interleaving(ccsds_rs_encoder::interleaving_depth d, size_t vfill = 0)
{
    fmt::print("test_interleaving {}\n", static_cast<size_t>(d));
    const size_t interleaving =
        static_cast<std::underlying_type_t<ccsds_rs_encoder::interleaving_depth>>(d);
    const size_t in_frame_len = interleaving * (223 - vfill);
    const size_t coded_frame_len = interleaving * (255 - vfill);

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(in_frame_len, 0);
    std::generate_n(data_in.begin(), in_frame_len, gen);
    std::vector<uint8_t> data_coded(coded_frame_len, 0);
    std::vector<uint8_t> data_decoded(in_frame_len, 0);

    auto enc = ccsds_rs_encoder::make(ccsds_rs_encoder::ecc::ecc16, d);
    auto dec = ccsds_rs_decoder::make(ccsds_rs_encoder::ecc::ecc16, d);
    enc->set_frame_size(in_frame_len);
    dec->set_frame_size(coded_frame_len);
    enc->generic_work(data_in.data(), data_coded.data());
    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

#if 0
BOOST_AUTO_TEST_CASE(all_depths_no_vfill)
{
    test_interleaving(ccsds_rs_encoder::interleaving_depth::depth1);
    test_interleaving(ccsds_rs_encoder::interleaving_depth::depth2);
    test_interleaving(ccsds_rs_encoder::interleaving_depth::depth3);
    test_interleaving(ccsds_rs_encoder::interleaving_depth::depth4);
    test_interleaving(ccsds_rs_encoder::interleaving_depth::depth5);
    test_interleaving(ccsds_rs_encoder::interleaving_depth::depth8);
}
#endif

} // namespace code
} // namespace satnogs
} // namespace gr
